#include "TrigOnlineMonitor/TrigROBMonitor.h"
#include "TrigOnlineMonitor/TrigMuCTPiROBMonitor.h"
#include "src/TrigALFAROBMonitor.h"
#include "src/TrigL1TopoWriteValData.h"

DECLARE_COMPONENT( TrigROBMonitor )
DECLARE_COMPONENT( TrigMuCTPiROBMonitor )
DECLARE_COMPONENT( TrigALFAROBMonitor )
DECLARE_COMPONENT( TrigL1TopoWriteValData )

